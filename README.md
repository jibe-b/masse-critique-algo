# Évaluation de masse critique

## Description du projet

### Constat

De nombreuses initiatives de transition locales (écologiques, sociales et démocratiques) ne décollent pas si elles sont entreprises de manière isolée/désynchronisée ou diluée.

Le seuil de masse critique à atteindre est différent suivant chaque cas.

### Hypothèse

En collectant les intentions de différentes personnes n'ayant pas par avance de contacts entre elles, on pourra identifier les masses critiques existantes mais diluées, amener les gens à échanger entre elles·eux,
et à passer à la mise en place de l'alternative concernée.

### Collecte

- collecte d'intentions à travers des questionnaires
- A/B testing : (via l'outil Airtables, différentes versions pour les mêmes questions sont proposées, ainsi que différents ordonnancement des questions)

### Création de communautés à partir des résultats

- par la suite, les questionnaires seront dépouillés / analysés
- dès qu'une masse critique est identifiée, invitation des personnes à se rassembler dans un chat et proposition de co-construction de dynamique collective

## Points d'entrée

Participez aux sondages en accédant aux points d'entrée suivants :

- Groupe facebook [Construisons ensemble des masses critiques pour les transitions locales](https://www.facebook.com/groups/2422276807871399/)
- Page facebook [Formons des masses critiques pour les transitions locales](https://www.facebook.com/Formons-des-masses-critiques-pour-les-transitions-locales-112115473590546)
- [Chat sur communecter.org](https://chat.communecter.org/channel/massesCritiquesTransition)

## Consultation des résultats

Via [cette app](https://jibe-b-masse-critique-algo.glitch.me/), vous pouvez accéder

- aux résultats des évaluations de masses critiques
- aux données (anonymisées et avec un embargo dégressif), accessibles en Open Data 

## Comment contribuer

- proposez des sujets de sondages
- proposez des versions de sondages
- proposez des améliorations aux algorithmes de détection de masses critiques
- les contacts collectés par sondage seront gardés secrets par respect de la vie privée ; toutefois vous pouvez faire savoir que vous pensez pouvoir activer une masse critique
- particiez à une dynamique lancée pour une masse critique atteinte !

### Modèle économique

- Si vous êtes porteur de projet, participez financièrement mensuellement pour une campagne de sondages auprès de votre public acquis, sans mise des données en Open Data
- Choisissez l'option "communauté" quand vous soutenez financièrement pour soutcnir la poursuite des campagnes de sondages vec un public non-restreint, mise des données en OpenData et animation des communautés une fois une masse critique atteinte.

Pour participer financièrement : https://opencollective.com/contribuons

### Algos

- Consulter le code ici : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jibe-b%2Fmasse-critique-algo/master?urlpath=lab%2Ftree%2Fmain.ipynb)

### Exécution

- Le code est exécuté à chaque modification de l'algo de calcul de la masse critique. Consulter les [derniers résultats de l'exécution](https://gitlab.com/jibe-b/masse-critique-algo/pipelines).
- L'app est développée sur Glitch : [projet](https://glitch.com/edit/#!/jibe-b-masse-critique-algo) avec un [mirroir sur Gitlab](https://gitlab.com/jibe-b/masse-critiquealgo-app-mirroir-de-glitch)
- Les envois des formulaires et des invitations à participer à une dynamique une fois la masse critique atteinte sont envoyés depuis [un compte mailjet](https://app.mailjet.com/account) et via messenger

### Données

- Les données les sondages sont dans le dossier [data](https://gitlab.com/jibe-b/masse-critique-algo/tree/master/data)
- Les résultats de l'exécution des algos d'évaluation de masse critique sont dans le dossier [résults](https://gitlab.com/jibe-b/masse-critique-algo/tree/master/results)

### J'ai trouvé une erreur…

Ouvrez un ticket dans le [gestionnaire de tickets](https://gitlab.com/jibe-b/masse-critique-algo/issues)